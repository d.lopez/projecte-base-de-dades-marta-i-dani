**CONFIGURACIÓ SGBD**

 

**ENUNCIAT**

 

Partint del SGBD Percona Server instal·lat en l'activitat anterior realitza aquests canvis en el fitxer de configuració.

 

 

**REALITZA LES SEGÜENTS TASQUES DE CONFIGURACIÓ i COMPROVACIÓ DE LOGS (6 punts)**

 

\1.    **Quins són els logs activats per defecte? Crea un fitxer de configuració anomenat logs.cnf a on:**

•         **Activa els logs que no ho estiguin per defecte i indica les configuracions necessàries per activar-los. Indica les rutes dels fitxer de log de Binary, Slow Query i General. Quins paràmetres has creat/modificat?**

 

![img](./imatges/ex1-1.png)

 

Podem trobar els logs d´errors en /var/log/mysqld.log

![img](./imatges/ex1-2.png)

![img](./imatges/ex1-3.png)

 

 

 

 

Per veure els logs generals creem un nou fitxer i escrivim la següent informació en el fitxer /etc/my.cnf

![img](./imatges/ex1-4.png)

Seguidament farem un *chown mysql:mysql /var/log/mysql_general.log*

 

 

Canviem els permisos

Reiniciem el servei

![img](./imatges/ex1-5.png)

![img](./imatges/ex1-6.png)

 

![img](./imatges/ex1-7.png)

 

BINARY LOG:

![img](./imatges/ex1-8.png)

Afegim al fitxer /etc/my.cnf el següent contingut

Reiniciem el mysql

 

**SLOW QUERY LOG:**

![img](./imatges/ex1-9.png)

![img](./imatges/ex1-10.png)

 

PROBLEMES QUE HEM TINGUT:

![img](./imatges/ex1-11.png)

Per defecte els logs generals i els slow query tenen la ubicació /var/lib/mysql , hem fet molts intents però no ens canviava cap fitxer dels logs.

 

**2.**    **Comprova l'estat de les opcions de log que has utilitzat mitjançant una sessió de mysql client.**

**Exemple: (mysql> SHOW GLOBAL VARIABLES LIKE '%log')**

 

![img](./imatges/ex2-1.png)

 

![img](./imatges/ex2-2.png)

**3.**    **Modifica el fitxer de configuració i desactiva els logs de binary, slow query i genral. Nota: Simplament desactiva'ls no borris altres paràmetres com la ruta dels fitxers, etc...**

![img](./imatges/ex3-1.png)

Si fiquem un 0 davant les línies general_log , slow_query_log en comptes d´un 1 desactivariem els logs reiniciant el servei.

Per desactivar els logs binaris afegiríem la línia **skip-login_bin = mysql-bin**

![img](./imatges/ex3-2.png)

Podem veure que els logs estan desactivats

 

\4.    **Activa els logs en temps d'execució mitjançant la sentència SET GLOBAL. També canvia el destí de log general a una taula (paràmetre log_output). Quines són les sentències que has utilitzat? A quina taula es guarden els dels del general log?**

![img](./imatges/ex4-1.png)

Els logs binaris no es poden activar ja que és una variable de lectura.

![img](./imatges/ex4-1.png)

![img](./imatges/ex4-2.png)

Per activar els logs binaris podem activar la variable sql_log_bin.

![img](./imatges/ex4-3.png)

 

 

**5.**    **Carrega la BD Sakila localitzada a la web de**

◦          **Descarrega't el fitxer sakila-schema.sql del Moodle.**

◦          **carrega la BD dins del MySQL utilitzant la sentència:**

**mysql> SOURCE <ruta_fitxer>/sakila-schema.sql;**

![img](./imatges/ex5-1.png)

 

**6.**    **Compte el numero de sentències CREATE TABLE dins del general log mitjançant una sentència SQL.**

◦          **Mostra quina sentència has utilitzat i mostra'n el resultat.**

![img](./imatges/ex6-1.png)

 

\7.     **Executa una query mitjançant la funció SLEEP(11) per tal de que es guardi en el log de Slow Query Log. Mostra el contingut del log demostrant-ho.**

![img](./imatges/ex7-1.png)

 

\8.    **Assegura't que el Binary Log estigui activat i borra tots els logs anteriors mitjançant la sentència RESET MASTER.**

•         **Crea i esborra una base de dades anomenada foo. Utilitza la sentències:**

​                        **mysql> CREATE DATABASE foo;**

​                        **mysql> DROP DATABASE foo;**

 

•         **Mitjançant la sentència SHOW BINLOG EVENTS llista els events i comprova les sentències anteriors en quin fitxer de log estan.**

 

•         **Realitza un rotate log mitjançant la sentència FLUSH LOGS. Què realitza exactament aquesta sentència?**

 

•         **Crea i esborra una altra base de dades com l'exemple anteior del foo. Però en aquest cas anomena la base de dades bar**

 

•         **Llista tots els fitxers de log i els últims canvis mitjançant la sentència SHOW. Quina sentència has utilitzat? Mostra'n el resultat.**

 

•         **Esborra el primer binary log. Quina sentència has utilitzat?**

 

•         **Utilitza el programa mysqlbinlog per mostrar el fitxer mysql-bin.000002**

◦          **Quin és el seu contingut?**

◦          **Quin número d'event ha estat el de la creació de la base de dades bar?**

 

**9.**    **De quina manera podem desactivar el binary log només d’una sessió en concret. Imagina’t que ets un administrador de la BD i no vols que les instruccions que facis es gravin en el binary_log.**

 

**CONFIGURACIÓ DEL SERVIDOR PERCONA SERVER PER REALITZAR CONNEXIONS SEGURES SOBRE SSL. (3 punts)0**

 

\1.    **Comprova mitjançant el programari WireShark que la connexió d'autentificació no és segura.**

**2.**    **Indica els passos que has realitzat per configurar el servidor i fer que un dels usuaris de MySQL es connection mitjançant SSL.**

 

**La seguent comanda s´utilitza per reduir el Firewall a l´entrada del port 3306 i podernos conectar per SSL** 

 

![img](./imatges/ex8-1.png)

Afegiriem el contingut al arxiu /etc/my.cnf

![img](./imatges/ex8-2.png)

Creem un usuari al mysql i al final fiquem REQUIRE SSL.

![img](./imatges/ex8-3.png)

![img](./imatges/ex8-4.png)

\3.    **Mostra el professor la configuració i la configuració**

 

 

**ENTREGA**

 

**Opcions d'entrega:**

\1.    Utilitzar aquest document com a plantilla per l’entrega de l’activitat i penjar-la en tasca destinada a aquesta activitat dins del curs Moodle

\2.    Realitzar la documentació sobre un repositori GIT (Bitbucket o GitLab). Utilitzant el format de fitxer MarkDown (MD). **(1 punt)**

\3.    Dins d'una WikiMedia. **(1 punt)**